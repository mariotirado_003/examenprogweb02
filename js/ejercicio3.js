
document.getElementById('imageInput').addEventListener('change', function () {
    const preview = document.getElementById('preview');
    const file = this.files[0];

    if (file) {
        preview.src = URL.createObjectURL(file);
    } else {
        // Si no se selecciona ninguna imagen, restablece la fuente de la imagen a un marcador de posición o vacía.
        preview.src = '';
    }
});
